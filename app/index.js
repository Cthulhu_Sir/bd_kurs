const express = require('express');
const app = express();
require('dotenv').config();
const path = require('path');
const ejs = require('ejs-locals');

const port = 3000;

const router = require('./routes');
app.use('/', router);

app.engine('ejs', ejs);
app.set('views', path.join(__dirname, 'pages'));
app.set('view engine', 'ejs');
app.use('/assets', express.static(__dirname + '/pages/assets'));

app.listen(port, (err) => {
    if(err){
        return console.log('something bad happened: ', err);
    }

    console.log(`server is listening on ${port}`);
});