const express = require('express');
const router = express.Router();
const dbc = require('./dbconnection');
const cookieParser = require('cookie-parser');
const bcrypt = require('bcrypt');
require('dotenv').config();
var pg = require('pg')
    , session = require('express-session')
    , pgSession = require('connect-pg-simple')(session);

var pgPool = new pg.Pool({
    user: process.env.PG_USER,
    host: process.env.PG_HOST,
    database: process.env.PG_DATABASE,
    password: process.env.PG_PASSWORD,
    port: process.env.PG_PORT
});

router.use(express.urlencoded({extended: true}));
router.use(session({
    store: new pgSession({
        pool: pgPool,                // Connection pool
        tableName: 'users_session'   // Use another table-name than the default "session" one
    }),
    secret: process.env.FOO_COOKIE_SECRET,
    resave: false,
    cookie: {maxAge: 30 * 24 * 60 * 60 * 1000} // 30 days
}));
router.use(cookieParser());
router.use(session({secret: process.env.FOO_COOKIE_SECRET}));


router.get('/', (request, response, next) => {
        // console.log(process.env.FOO_COOKIE_SECRET);
        // console.log("Openned: " + request.session.email + " " + request.session.authorized);
        if (request.session.authorized !== true) {
            next();
        } else {
            response.render('index.ejs', {auth: true, email: request.session.email});
        }
    },
    (request, response) => {
        response.render('index', {auth: false, email: undefined});
    });

router.post('/authUser', (request, response) => {
    dbc.getUser(request.body.email).then(res => {
        if (res !== undefined) {
            console.log("Logging in: " + request.body.email + " " + request.body.password);
            let pass = res.password;
            if (bcrypt.compareSync(request.body.password, pass)) {
                console.log(request.body.email + " logged in");
                request.session.authorized = true;
                request.session.email = request.body.email;
                response.json({auth: true, email: request.body.email});
            } else {
                console.log(request.body.email + " can't log in");
                response.json({auth: false});
            }
        } else {
            console.log(request.body.email + " can't log in");
            response.json({auth: false});
        }
    }).catch(err => {
        console.log(err);
        console.log(request.body.email + " can't log in");
        response.json({auth: false});
    });
});

router.post('/regUser', (request, response, next) => {
        const emailReg = new RegExp('^[-a-z0-9!#$%&\'*+/=?^_`{|}~]+(?:\\.[-a-z0-9!#$%&\'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$');
        const passReg = new RegExp('(?=.{8,})');
        if (request.body.email.toString().match(emailReg) && request.body.password.toString().match(passReg)) {
            next();
        } else {
            console.log("incorrect email or pass: " + request.body.email + " : " + request.body.email.toString().match(emailReg) + " : " + request.body.password + " : " + request.body.password.toString().match(passReg));
            response.json({
                auth: false,
                errorMessage: "Некорректная почта или пароль. Пароль должен быть минимум 8 знаков"
            });
        }
    },
    (request, response) => {
        console.log("Registering: " + request.body.email + " " + request.body.password);
        dbc.addUser(request.body.email, request.body.password).then(res => {
            console.log(request.body.email + " registered");
            request.session.authorized = true;
            request.session.email = request.body.email;
            response.json({auth: true, email: request.body.email});
        }).catch(res => {
            console.log(request.body.email + " can't register");
            response.json({auth: false, errorMessage: "Такой пользователь уже существует"});
        })
    });

router.post('/logout', (request, response) => {
    console.log("User log out: " + request.body.email);
    request.session.authorized = false;
    request.session.email = undefined;
    request.session.destroy();
    response.json({auth: false});
});

router.get('/constructor', (request, response) => {
    let ingredients = undefined;
    let categories = undefined;

    dbc.getIngredients().then(async (res) => {
        ingredients = await res;
        console.log(ingredients);
        dbc.getCategories().then(async (res1) => {
            categories = await res1;
            console.log(categories);
            response.render('constructor', {
                auth: request.session.authorized,
                email: request.session.email,
                ing: ingredients,
                cat: categories
            })
        }).catch(e => {
        });
    }).catch(e => {
    });

});

router.post('/addNewPizza', (request, response) => {
    let doughCount = 0;
    for (let ing of request.body.ingredients) {
        console.log(ing);
        if (ing.category === 'Тесто') {
            doughCount++;
        }
    }
    if (request.session.email === undefined) {
        response.json({
            err: 'Необходимо авторизоваться'
        });
        return;
    }
    console.log(doughCount);
    if (doughCount !== 1) {
        response.json({
            err: 'Необходимо выбрать ровно одну основу для пиццы'
        });
        return;
    }
    if (request.body.name === '') {
        response.json({
            err: 'Необходимо указать имя пиццы'
        });
        return;
    }
    if (request.body.ingredients.length < 2) {
        response.json({
            err: 'У вас всего один игредиент, это ещё не пицца'
        });
        return;
    }
    // console.log(request.session.email);
    dbc.addPizza(request.body.name, request.body.ingredients, request.session.email).then(res => {
        console.log(res);
        response.json({
            pizza_id: res
        });
        return;
    }).catch(e => {
        response.json({
            err: 'Что-то не так'
        });
        return;
    })

});

router.post('/addToCart', (request, response) => {
    // console.log('Imma here!');
    dbc.addToCart(request.session.email, request.body.pizza_id).then((res) => {
        console.log(res);
        response.json({
            success: res
        });
    }).catch(e => {
        // console.log("TUPO!!");
        console.error(e.stack);
        throw e;
    })
});

router.post('/getUserCart', (request, response) => {
    if (request.session.authorized) {
        dbc.getUserCart(request.session.email).then(res => {
            // console.log(res);
            response.json({
                content: res
            })
        }).catch(e => {
            console.log(e.stack);
        });
    } else {
        response.redirect('/');
    }
});

router.post('/removeFromCart', (requset, response) => {
    dbc.removeFromCart(requset.session.email, requset.body.id).then(res => {
        response.json({
            id: res
        })
    }).catch(e => {
        console.log(e.stack);
    })
});

router.get('/ratingPage', (request, response) => {
    dbc.getAllPizzas().then(res => {
        response.render('rating', {
            res: res,
            auth: request.session.authorized,
            email: request.session.email
        });
    }).catch(e => {
        response.render('rating', {res: "Whoops!"})
    })
});

router.get('/orders', (request, response) => {
    if (request.session.authorized) {
        dbc.getUserOrders(request.session.email).then(res => {
            // console.log(res);
            response.render(('orders'), {
                auth: request.session.authorized,
                email: request.session.email,
                res: res
            });
        });
    } else {
        response.redirect('/');
    }
});

router.post('/addScore', (request, response) => {
    dbc.addScore(request.session.email, request.body.id).then(res => {
        response.json({
            sucess: true
        });
    }).catch(e => {
        console.error(e.stack);
        throw e;
    })
});

router.post('/removeScore', (request, response) => {
    dbc.removeScore(request.session.email, request.body.id).then(res => {
        response.json({
            sucess: true
        });
    }).catch(e => {
        console.error(e.stack);
        throw e;
    })
});

router.get('/complete-order', (request, response) => {
    if (request.session.authorized) {
        dbc.getUserCart(request.session.email).then(res => {
            // console.log(res);
            response.render('completeOrder', {
                auth: request.session.authorized,
                email: request.session.email,
                res: res
            })
        }).catch(e => {
            console.log(e.stack);
        });
    } else {
        response.redirect('/');
    }
});

//TODO 'add 404 page'

router.post('/addNewOrder',(request, response) => {
    // console.log(request.body);
    dbc.addNewOrder(request.session.email, request.body).then(res => {
        dbc.getOrder(res.id).then(ord => {
            response.render('orderinfo', {
                auth: request.session.authorized,
                email: request.session.email,
                res: ord
            })
        }).catch(e => {
            console.error(e.stack);
            throw e;
        })
    }).catch(e => {
        console.error(e.stack);
        throw e;
    });
});

module.exports = router;