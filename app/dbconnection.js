const {Pool, Client} = require('pg');
require('dotenv').config();

const pool = new Pool({
    user: process.env.PG_USER,
    host: process.env.PG_HOST,
    database: process.env.PG_DATABASE,
    password: process.env.PG_PASSWORD,
    port: process.env.PG_PORT
});

const bcrypt = require('bcrypt');

async function getUser(email) {
    return await pool.query("SELECT * FROM users WHERE users.email like $1::varchar", [email]).then(async res => {
        console.log(res);
        return res.rows[0];
    })
        .catch(e => {
            console.error("On getUser: ");
            console.error(e.stack);
            throw e;
        })
}

async function addUser(email, password) {

    const salt = bcrypt.genSaltSync(10);
    const passwordToSave = bcrypt.hashSync(password, salt);


    return await pool.query("INSERT INTO users (email, password) VALUES ($1::varchar, $2::varchar) RETURNING *", [email, passwordToSave]).then(res => {
        return res.rows[0];
    })
        .catch(e => {
            console.error("On addIngredients: ");
            console.error(e.stack);
            throw e;
        })
}

async function getIngredients() {
    return await pool.query("SELECT name, category, price FROM ingredients ORDER BY name").then((res) => {
        // console.log(res.rows);
        return res.rows;
    }).catch((e) => {
        console.error("On getIngredients: ");
        console.error(e.stack);
        throw e;
    })
}

async function getCategories() {
    return await pool.query("SELECT * from categories").then(res => {
        return res.rows;
    }).catch(e => {
        console.error("On getCategories: ");
        console.error(e.stack);
        throw e;
    })
}

async function addPizza(name, ingredients, email) {
    return await calculatePrice(ingredients).then(price => {
        return pool.query("INSERT INTO pizzas (name, owner, price) VALUES ($1::varchar, $2::varchar, $3::integer) RETURNING id", [name, email, price]).then((res) => {
            console.log(res.rows[0]);
            addedPizza = res.rows[0].id;
            for (ing of ingredients) {
                pool.query("INSERT INTO pizzas_ingredients (pizza, ingredient) VALUES ($1::integer, $2::varchar)", [res.rows[0].id, ing.name]).catch(e => {
                    throw e;
                });
            }
            return res.rows[0].id;
        }).catch(e => {
            throw e;
        });
    }).catch(e => {
        console.error("On addPizza: ");
        console.error(e.stack);
        throw e;
    });
}

async function getUserId(email) {
    // console.log('getting user id. Email: ' + email);
    return await pool.query("SELECT id FROM users WHERE email like $1::varchar", [email]).then((res) => {
        // console.log(res.rows[0]);
        return res.rows[0].id;
    }).catch(e => {
        // console.log('ОШЫБКА');
        console.error("On GetUserId: ");
        console.error(e.stack);
        throw e;
    })
}

//ADD OFFSET
async function getAllPizzas() {
    let pizzaInfo = await pool.query('SELECT * FROM pizzas ORDER BY RATING DESC').then(res => {
        return res.rows;
    }).catch(e => {
        console.error("On getAllPizzas: ");
        console.error(e.stack);
        throw e;
    });

    for (pizza of pizzaInfo) {
        pizza.ingredients = await getPizzaIngredients(pizza.id);
    }

    // console.log(pizzaInfo);
    return pizzaInfo;
}

async function getPizzaIngredients(pizzaId) {
    return await pool.query('SELECT * FROM (pizzas_ingredients p join ingredients i on p.ingredient=i.name) where p.pizza=$1::integer', [pizzaId]).then(res => {
        return res.rows;
    }).catch(e => {
        console.error("On getPizzaIngredients: ");
        console.log(e.stack);
        throw e;
    })
}

async function calculatePrice(ingredients) {
    let price = 0;

    for (ing of ingredients) {
        await pool.query("SELECT price FROM ingredients WHERE name like $1::varchar", [ing.name]).then(res => {
            // console.log(res.rows);
            price += res.rows[0].price;
            // console.log(price);
        }).catch(e => {
            console.error("On calculatePrice: ");
            console.error(e.stack);
            throw e;
        });
    }
    return price;
}

async function addToCart(email, pizzaId) {
    return await pool.query("INSERT INTO carts_content (owner, pizza) VALUES ($1::varchar, $2::integer)", [email, pizzaId]).then(res => {
        // console.log(res);
        return true;
    }).catch(e => {
        console.error("On addToCart: ");
        console.error(e.stack);
        throw e;
    });
}

async function getUserCart(email) {
    let pizzaInfo = await pool.query('SELECT p.id, p.name, p.price, c.amount FROM (pizzas p join carts_content c on c.pizza=p.id) WHERE c.owner like $1::varchar', [email]).then(res => {
        return res.rows;
    }).catch(e => {
        console.error("On GetUserCart: ");
        console.error(e.stack);
        throw e;
    });

    for (pizza of pizzaInfo) {
        pizza.ingredients = await getPizzaIngredients(pizza.id);
    }

    // console.log(pizzaInfo);
    return pizzaInfo;
}

async function removeFromCart(email, id) {
    let removedId = await pool.query('DELETE FROM carts_content WHERE owner like $1::varchar AND pizza=$2::integer RETURNING pizza', [email, id]).then(res => {
        if (res.rows[0] === undefined) {
            return id;
        }
        return res.rows[0].pizza;
    }).catch(e => {
        console.error("On removeFromCart: ");
        console.error(e.stack);
        throw e;
    });


    return removedId;
}

async function getUserOrders(email) {
    let pizzaInfo = await pool.query("SELECT o.id order_id, p.id, p.name, p.rating, p.price, oc.amount, scored FROM (((orders o join orders_content oc on oc.id=o.id) join pizzas p on oc.pizza=p.id) left join scores rs on o.owner=rs.user and oc.pizza=rs.pizza) WHERE o.owner like $1::varchar ORDER BY o.delivery_time DESC", [email]).then(res => {
        // console.log(res.rows);
        return res.rows;
    }).catch(e => {
        console.error("On getUserOrders: ");
        console.error(e.stack);
        throw e;
    });


    for (pizza of pizzaInfo) {
        pizza.ingredients = await getPizzaIngredients(pizza.id);
    }

    let ordersIDs = await pool.query("SELECT * FROM orders WHERE owner like $1::varchar ORDER BY delivery_time DESC", [email]).then(res => {
        return res.rows;
    }).catch(e => {
        console.error("On getUserOrders: ");
        console.error(e.stack);
        throw e;
    });

    pizzaInfo.orders = ordersIDs;

    // console.log(pizzaInfo);
    return pizzaInfo;
}

async function addScore(email, pizzaId) {
    return await pool.query("INSERT INTO Scores VALUES ($1::varchar, $2::integer) RETURNING pizza", [email, pizzaId]).then(res => {
        return res.rows[0];
    }).catch(e => {
        console.error("On addScore: ");
        console.error(e.stack);
        throw e;
    })
}

async function removeScore(email, pizzaId) {
    // console.log(email, pizzaId);
    return await pool.query("DELETE FROM scores WHERE scores.user like $1::varchar AND pizza=$2::integer RETURNING pizza", [email, pizzaId]).then(res => {
        console.log(res);
        return res.rows[0];
    })
}

async function addNewOrder(email, info) {
    let content = await getUserCart(email);
    let queryString =
        "WITH o as (INSERT INTO orders (owner, address, phone, name) VALUES ($1::varchar, $2::varchar, $3::varchar, $4::varchar) RETURNING id)" +
        "INSERT " +
        "INTO orders_content (id, pizza, amount)" +
        "VALUES ";
    let i = 5;
    let contentArray = [];
    contentArray.push(email);
    contentArray.push(info.address);
    contentArray.push(info.phone);
    contentArray.push(info.name);
    content.forEach(pizza => {
        queryString += "((SELECT id FROM o), $" + i + "::integer, $";
        i++;
        queryString += i + "::integer),";
        console.log(pizza);
        i++;
        contentArray.push(pizza.id);
        contentArray.push(pizza.amount);
    });
    queryString = queryString.slice(0, -1);
    queryString += " RETURNING id";

    console.log(queryString);
    // console.log(contentArray);
    let queryResult = await pool.query(queryString, contentArray).then(res => {
        // console.log(res.rows);
        return res.rows[0];
    }).catch(e => {
        console.error("On addNewOrder: ");
        console.error(e.stack);
        throw e;
    });

    let deleteResult = await pool.query("SELECT clear_cart($1::varchar)", [email]).then(res => {
        // console.log(res.rows);
        return res.rows;
    }).catch(e => {
        console.error("On addNewOrder: ");
        console.error(e.stack);
        throw e;
    });

    console.log(queryResult);
    return queryResult;
}

async function getOrder(orderId) {
    console.log(orderId);
    let queryResult = await pool.query("SELECT * FROM orders WHERE id=$1::integer", [orderId]).then(res => {
        console.log(res.rows);
        return res.rows;
    }).catch(e => {
        console.error("On getOrder: ");
        console.error(e.stack);
        throw e;
    });
    return queryResult;
}

module.exports = {
    pool,
    getUser,
    addUser,
    getIngredients,
    getCategories,
    addPizza,
    getAllPizzas,
    addToCart,
    getUserCart,
    removeFromCart,
    getUserOrders,
    addScore,
    removeScore,
    addNewOrder,
    getOrder
};
