function addToCart(id) {

    $.ajax({
        type: 'POST',
        url: 'addToCart',
        data: {
            pizza_id: id
        },
        dataType: 'json',
        success: function (dat) {
            console.log(dat);
            if(dat.success){
                location.reload(true);
            }
        }
    })
}