$('#navBarConstructorButton').attr('class', 'nav-item active');
$('#Тесто-tab').attr('class', 'nav-link active');
$('#Тесто').attr('class', 'tab-pane fade show active');

$('.ingButton').change(function (event) {
    let sum = parseInt($('#priceSummary').html());
    let currentElPrice = parseInt($(event.currentTarget).parents('.ingredient-table-row').children('.ingredient-price').children('span').html());

    if ($(event.currentTarget).prop('checked')) {
        $('#priceSummary').html(sum + currentElPrice);

    } else {
        $('#priceSummary').html(sum - currentElPrice);
    }

});

function addNewPizza() {

    let ingredientsArray = [];
    $('.ingButton').each((iter) => {
        // console.log($('.ingButton')[btn]);
        if ($($('.ingButton')[iter]).prop('checked')) {
            let ingr = {
                id: $($('.ingButton')[iter]).attr('id'),
                name: $($('.ingButton')[iter]).parents('.ingredient-table-row').children('.ingredient-name').children('span').html(),
                // category: $($('.ingButton')[btn]).parents('.ingredient-table-row').attr('id'),
                category: $($('.ingButton')[iter]).parents('tbody').attr('cat_id')
            };
            ingredientsArray.push(ingr);
        }
    });

    console.log(ingredientsArray);
    console.log($('#pizza-name').val());

    $.ajax({
        type: 'POST',
        url: 'addNewPizza',
        data: {
            name: $('#pizza-name').val(),
            ingredients: ingredientsArray
        },
        dataType: 'json',
        success: function (data) {
            if (data.err) {
                console.log(data.err);
            } else {
                console.log(data.pizza_id);

                $.ajax({
                    type: 'POST',
                    url: 'addToCart',
                    data: {
                        pizza_id: data.pizza_id
                    },
                    dataType: 'json',
                    success: function (dat) {
                        console.log(dat);
                        if(dat.success){
                            location.reload(true);
                        }
                    }
                })

            }
        },
        error: function (xhr, str) {

        }
    });

}