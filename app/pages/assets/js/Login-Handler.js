function loginButtonPressed() {
    // console.log($('#formx'));
    var msg = $('#formx').serialize();
    // console.log(msg);
    $.ajax({
        type: 'POST',
        url: 'authUser',
        data: msg,
        dataType: 'json',
        success: function (data) {
            $('#errorParag').remove();
            if (data.auth === true) {
                location.reload(true);
            } else {
                $('#loginModalFooter').append('<p id="errorParag"></p>');
                $('#errorParag').html("Неправильный логин или пароль");
                $('#errorParag').css('color', 'red');
            }
        },
        error: function (xhr, str) {
            $('#errorParag').remove();
            $('#loginModalFooter').append('<p id="errorParag"></p>');
            $('#errorParag').html("Что-то пошло не так(");
            $('#errorParag').css('color', 'red');
        }
    });

}

function regButtonPressed() {
    var msg = $('#formx').serialize();
    $.ajax({
        type: 'POST',
        url: 'regUser',
        data: msg,
        dataType: 'json',
        success: function (data) {
            $('#errorParag').remove();
            if (data.auth === true) {
                location.reload(true);
            } else {
                $('#loginModalFooter').append('<p id="errorParag"></p>');
                $('#errorParag').html(data.errorMessage);
                $('#errorParag').css('color', 'red');
            }
        },
        error: function (xhr, str) {
            console.log(xhr);
            $('#errorParag').remove();
            $('#loginModalFooter').append('<p id="errorParag"></p>');
            $('#errorParag').html("Что-то пошло не так(");
            $('#errorParag').css('color', 'red');
        }
    });
}

function logout() {
    console.log("logout");
    $.ajax({
        type: 'POST',
        url: 'logout',
    }).then(() => {
        location.reload(true);
    }).catch(e => console.error(e.stack));


}

function addDropdownAccount(email) {
    $('#navcol-1').append('                <div class="btn-group" id="accountDropdownGroup">\n' +
        '                    <button type="button" id="accountDropdown" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
        '                        Аккаунт\n' +
        '                    </button>\n' +
        '                    <div class="dropdown-menu" id="accountDropdownMenu">\n' +
        '                        <a class="dropdown-item" href="#">' + email + '</a>\n' +
        '                        <a class="dropdown-item" onclick="logout()">Выйти</a>\n' +
        '                    </div>\n' +
        '                </div>')
}

function getUserCart() {
    $.ajax({
        type: 'POST',
        url: '/getUserCart',
        dataType: 'json',
        success: function (data) {
            let cart = $('#user-cart');
            data.content.forEach(pizza => {
                cart.append("<tr id='" + pizza.id + "'><th>" + pizza.name + "</th><th class='price-field'>" + pizza.price + "р</th>" + "<th class='amount' id='" + pizza.id + "-amount'>" + pizza.amount + "шт.</th>" +
                    "<th><button type=\"button\" class=\"btn btn-primary\" onclick='removeFromCart(+" + pizza.id + ")'>-</button></th></tr>")
            });
            $('#total-price').html(getTotalPrice() + "р");
            $('#order-total-price').html(getTotalPrice() + "р");
        }
    })
}

function removeFromCart(id) {
    $.ajax({
        type: 'POST',
        url: '/removeFromCart',
        data: {
            id: id
        },
        dataType: 'json',
        success: function (data) {
            console.log(data);
            if (parseInt($('#' + data.id + '-amount').html()) > 1) {
                $($('#' + data.id + '-amount')).html(parseInt($('#' + data.id + '-amount').html())-1 + "шт.");
                $('#order-' + data.id + '-amount').html(parseInt($('#' + data.id + '-amount').html())-1 + "шт.");
                $('#total-price').html(getTotalPrice() + "р");
                $('#order-total-price').html(getTotalPrice() + "р");
            } else {
                $('#order-' + data.id).first().remove();
                $('#' + data.id).first().remove();
                $('#total-price').html(getTotalPrice() + "р");
                $('#order-total-price').html(getTotalPrice() + "р");
            }
        }
    })
}

function getTotalPrice() {
    let total = 0;
    $('.price-field').each(iter => {
        let target = $($('.price-field')[iter]);
        let price = parseInt($($('.price-field')[iter]).html());
        let amount = parseInt(target.parent().find('.amount').html());
        console.log(price);
        console.log(amount);
        total += price*amount;
    });
    return total;
}

$('#cart-button').load(getUserCart());
