$('.favorite').click(function (event) {

    if ($(event.target).hasClass('checked')) {
        $('.' + $(event.target).attr('id')).attr('src', 'assets/img/star-empty.svg');
        $('.' + $(event.target).attr('id')).removeClass('checked');
        $.ajax({
            type: 'POST',
            url: '/removeScore',
            data: {
                id: $(event.target).attr('id')
            },
            dataType: 'json',
            success: function (data){

            }
        })
    } else {
        // console.log($(event.target).attr('id'));
        let targetId = $(event.target).attr('id');
        // console.log(targetId);
        let target = $('.' + targetId);
        // console.log(target);
        target.attr('src', 'assets/img/star.svg');
        target.addClass('checked');
        $.ajax({
            type: 'POST',
            url: '/addScore',
            data: {
                id: $(event.target).attr('id')
            },
            dataType: 'json',
            success: function (data){

            }
        })
    }
});