const {Pool, Client} = require('pg');
require('dotenv').config();

const pool = new Pool({
    user: process.env.PG_USER,
    host: process.env.PG_HOST,
    database: process.env.PG_DATABASE,
    password: process.env.PG_PASSWORD,
    port: process.env.PG_PORT
});

const bcrypt = require('bcrypt');

async function getUser(email) {
    return await pool.query("SELECT * FROM Users WHERE Users.email like $1::varchar", [email]).then(async res => {

        return res.rows[0];
    })
        .catch(e => {
            console.error(e.stack);
            throw e;
        })
}

async function addUser(email, password) {

    const salt = bcrypt.genSaltSync(10);
    const passwordToSave = bcrypt.hashSync(password, salt);


    return await pool.query("INSERT INTO Users (email, password, name) VALUES ($1::varchar, $2::varchar, 'boop') RETURNING *", [email, passwordToSave]).then(res => {
        // console.log(res.rows[0])
        return res.rows[0];
    })
        .catch(e => {
            console.error(e.stack);
            throw e;
        })
}

async function getIngredients() {
    return await pool.query("SELECT i.id, i.name, i.price, c.id cid, c.name_ru cat FROM (ingredients i join ingredients_category c on i.category_id=c.id) ORDER BY i.id").then((res) => {
        // console.log(res.rows);
        return res.rows;
    }).catch((e) => {
        console.error(e.stack);
        throw e;
    })
}

async function getCategories() {
    return await pool.query("SELECT * from ingredients_category ORDER BY id").then(res => {
        return res.rows;
    }).catch(e => {
        console.error(e.stack);
        throw e;
    })
}

async function addPizza(name, ingredients, ownerEMail) {
    return getUserId(ownerEMail).then((res) => {
        return calculatePrice(ingredients).then(price => {
            // console.log(price);
            return pool.query("INSERT INTO pizza (name, owneruserid, price) VALUES ($1::varchar, $2::integer, $3::integer) RETURNING id", [name, res, price]).then((res) => {
                addedPizza = res.rows[0].id;
                // console.log(res.rows[0].id);
                // console.log(addedPizza);
                for (ing of ingredients) {
                    pool.query("INSERT INTO pizza_ingredients (pizza_id, ingredient_id) VALUES ($1::integer, $2::integer)", [res.rows[0].id, ing.id]).catch(e => {
                        throw e;
                    });
                }
                return res.rows[0].id;
            }).catch(e => {
                throw e;
            });
        }).catch(e => {
            throw e;
        });
    }).catch(e => {
        console.error(e.stack);
        throw e;
    });
}

async function getUserId(email) {
    // console.log('getting user id. Email: ' + email);
    return await pool.query("SELECT id FROM users WHERE email like $1::varchar", [email]).then((res) => {
        // console.log(res.rows[0]);
        return res.rows[0].id;
    }).catch(e => {
        // console.log('ОШЫБКА');
        console.error(e.stack);
        throw e;
    })
}

async function getAllPizzas() {
    let pizzaInfo = await pool.query('SELECT * FROM pizza ORDER BY RATING DESC').then(res => {
        return res.rows;
    }).catch(e => {
        console.error(e.stack);
        throw e;
    });

    for (pizza of pizzaInfo) {
        pizza.ingredients = await getPizzaIngredients(pizza.id);
    }

    // console.log(pizzaInfo);
    return pizzaInfo;
}

async function getPizzaIngredients(pizzaId) {
    return await pool.query('SELECT * FROM (pizza_ingredients p join ingredients i on p.ingredient_id=i.id) where p.pizza_id=$1::integer', [pizzaId]).then(res => {
        return res.rows;
    }).catch(e => {
        console.log(e.stack);
        throw e;
    })
}

async function calculatePrice(ingredients) {
    let price = 0;

    for (ing of ingredients) {
        await pool.query("SELECT price FROM ingredients WHERE id=$1::integer", [ing.id]).then(res => {
            // console.log(res.rows);
            price += res.rows[0].price;
            // console.log(price);
        }).catch(e => {
            console.error(e.stack);
            throw e;
        });
    }
    return price;
}

async function addToCart(email, pizzaId) {
    let userID = await getUserId(email);
    return await pool.query("INSERT INTO cart (owneruserid, pizza_id) VALUES ($1::integer, $2::integer)", [userID, pizzaId]).then(res => {
        // console.log(res);
        return true;
    }).catch(e => {
        console.error(e.stack);
        throw e;
    });
}

async function getUserCart(email) {
    let userID = await getUserId(email);
    let pizzaInfo = await pool.query('SELECT p.id, p.name, p.price FROM (pizza p join cart c on c.pizza_id=p.id) WHERE c.owneruserid=$1::integer', [userID]).then(res => {
        return res.rows;
    }).catch(e => {
        console.error(e.stack);
        throw e;
    });

    for (pizza of pizzaInfo) {
        pizza.ingredients = await getPizzaIngredients(pizza.id);
    }

    // console.log(pizzaInfo);
    return pizzaInfo;
}

async function removeFromCart(email, id) {
    let userID = await getUserId(email);
    let removedId = await pool.query('WITH i as (SELECT id FROM cart WHERE owneruserid=$1::integer AND pizza_id=$2::integer LIMIT 1) DELETE FROM cart AS c USING i WHERE c.id=i.id RETURNING pizza_id', [userID, id]).then(res => {
        return res.rows[0].pizza_id;
    }).catch(e => {
        console.error(e.stack);
        throw e;
    });


    return removedId;
}

async function getUserOrders(email) {
    let userID = await getUserId(email);
    // console.log(userID);
    // console.log('boop');
    let pizzaInfo = await pool.query("SELECT o.id order_id, p.id, p.name, p.rating, p.price, rs.score FROM (((orders o join orders_content oc on oc.order_id=o.id) join pizza p on oc.pizza_id=p.id) left join rating_scores rs on o.user_id=rs.user_id and oc.pizza_id=rs.pizza_id) WHERE o.user_id=$1::integer ORDER BY o.delivery_time DESC", [userID]).then(res => {
        // console.log(res.rows);
        return res.rows;
    }).catch(e => {
        console.error(e.stack);
        throw e;
    });


    for (pizza of pizzaInfo) {
        pizza.ingredients = await getPizzaIngredients(pizza.id);
    }

    let ordersIDs = await pool.query("SELECT * FROM orders WHERE user_id=$1::integer ORDER BY delivery_time DESC",[userID]).then(res => {
       return res.rows;
    }).catch(e => {
        console.error(e.stack);
        throw e;
    });

    pizzaInfo.orders = ordersIDs;

    // console.log(pizzaInfo);
    return pizzaInfo;
}

async function addScore(email, pizzaId){
    let userID = await getUserId(email);
    return await pool.query("INSERT INTO rating_scores VALUES (1, $1::integer, $2::integer) RETURNING pizza_id", [userID, pizzaId])
}

async function removeScore(email, pizzaId){
    let userID = await getUserId(email);
    // console.log(userID + " " + pizzaId);
    return await pool.query("DELETE FROM rating_scores WHERE user_id=$1::integer AND pizza_id=$2::integer RETURNING pizza_id", [userID, pizzaId])
}

async function addNewOrder(email, info){
    let content = await getUserCart(email);
    let userID = await getUserId(email);
    let queryString =
        "WITH o as (INSERT INTO orders (user_id, adress, phone, name) VALUES ($1::integer, $2::varchar, $3::varchar, $4::varchar) RETURNING id)" +
        "INSERT " +
        "INTO orders_content (order_id, pizza_id)" +
        "VALUES ";
    let i = 5;
    let contentArray = [];
    contentArray.push(userID);
    contentArray.push(info.adress);
    contentArray.push(info.phone);
    contentArray.push(info.name);
    content.forEach(pizza => {
        queryString += "((SELECT id FROM o), $" + i + "::integer),";
        i++;
        contentArray.push(pizza.id)
    });
    queryString = queryString.slice(0, -1);
    queryString += " RETURNING order_id";

    let queryResult = await pool.query(queryString, contentArray).then(res => {
        // console.log(res.rows);
        return res.rows[0];
    }).catch(e => {
        console.error(e.stack);
        throw e;
    });

    let deleteResult = await pool.query("DELETE FROM cart WHERE owneruserid=$1::integer", [userID]).then(res => {
        // console.log(res.rows);
        return res.rows;
    }).catch(e => {
        console.error(e.stack);
        throw e;
    });

    return queryResult;
}

async function getOrder(orderId){
    console.log(orderId);
    let queryResult = await pool.query("SELECT * FROM orders WHERE id=$1::integer", [orderId]).then(res => {
        console.log(res.rows);
        return res.rows;
    }).catch(e => {
        console.error(e.stack);
        throw e;
    });
    return queryResult;
}

module.exports = {
    pool,
    getUser,
    addUser,
    getIngredients,
    getCategories,
    addPizza,
    getAllPizzas,
    addToCart,
    getUserCart,
    removeFromCart,
    getUserOrders,
    addScore,
    removeScore,
    addNewOrder,
    getOrder
};
