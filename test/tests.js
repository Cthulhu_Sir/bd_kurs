const assert = require('assert');
const chai = require('chai');
const expect = chai.expect;

const dbc = require('../app/dbconnection');

describe('DBC', function () {
    describe('#getUser(email)', function () {
        it('should reply with undefined when the value is not present', async function () {
            let res = await dbc.getUser();
            console.log(res);
            expect(res).to.be.undefined;
        });
        it('should reply with user info when the value is present', async function () {
            let param = 'kukuluu.steam@yandex.ru';
            let res = await dbc.getUser(param);
            expect(res).to.have.all.keys('id', 'email', 'password');
            expect(res.email).to.equal(param);
        })
    });
    describe('#getUserOrders' ,function () {
       it('should reply with undefined when email is incorrect', async function() {
           let res = await dbc.getUserOrders().catch(e => {
           });
           expect(res).to.be.undefined;

       });
        it('should reply with array when email is correct', async function() {
            let param = 'kukuluu.steam@yandex.ru';
            let res = await dbc.getUserOrders(param);
            // console.log(res);
            expect(res).to.be.an('array').which.is.not.empty;
            expect(res).to.have.any.keys('orders');
        });
    });
});